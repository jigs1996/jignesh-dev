import Link from '@/components/Link'
import Tag from '@/components/Tag'
import siteMetadata from '@/data/siteMetadata'
import { formatDate } from 'pliny/utils/formatDate'
import NewsletterForm from 'pliny/ui/NewsletterForm'
import Image from '@/components/Image'

const MAX_DISPLAY = 5

export default function Home({ posts }) {
  return (
    <>
      <div className="divide-y divide-gray-200 dark:divide-gray-700">
        <div className="space-y-2 pb-8 pt-6 md:space-y-5">
          <h1 className="text-2xl font-extrabold leading-9 tracking-tight text-gray-900 dark:text-gray-100 sm:text-4xl sm:leading-10 md:text-5xl md:leading-14">
            Hi 👋, I'm {siteMetadata.author}
          </h1>
          <p className="text-lg leading-7 text-gray-500 dark:text-gray-400">
            {siteMetadata.description}
          </p>
          <p className="dark:text-grey text-gray mb-8   mt-4 text-sm">
            <span className="mr-3 inline-block whitespace-nowrap pt-3">💻 Javascript </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">🌿 NodeJS </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">⚛️ React.js </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">🚀 Next.js </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">🐘 PHP </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">📝 WordPress </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">🏗️ Laravel </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">🖼️ Vue.js </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">🌌 Nuxt.js </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">🛠️ HTML </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">🎨 CSS </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">✨ Tailwind </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">🔀 Git </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">🕸️ web3 </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">⛓️ blockchain </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">🌱 MongoDB </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">🐳 MySQL </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">🔧 APIs/SDKs </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">🌥️ AWS/GPC/DO </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">🌩️ Cloudflare </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">🌐 Apache/Nginx </span>
            <span className="mr-3 inline-block whitespace-nowrap pt-3">📊 VsCode </span>
          </p>
        </div>
        <ul className="divide-y divide-gray-200 dark:divide-gray-700">
          {!posts.length && 'No posts found.'}
          {posts.slice(0, MAX_DISPLAY).map((post) => {
            const { slug, date, title, images, summary, tags } = post
            return (
              <li key={slug} className="py-12">
                <article>
                  <div className="space-y-2 xl:grid xl:grid-cols-4 xl:items-baseline xl:space-y-0">
                    <div>
                      <dl>
                        <dt className="sr-only">Published on</dt>
                        <dd className="text-base font-medium leading-6 text-gray-500 dark:text-gray-400">
                          <time dateTime={date}>{formatDate(date, siteMetadata.locale)}</time>
                        </dd>
                      </dl>
                      <div className="py-2 pr-3">
                        {images && images.length > 0 && (
                          <Image
                            alt={title}
                            src={images[0]}
                            className="object-cover object-center"
                            width={215}
                            height={150}
                          />
                        )}
                      </div>
                    </div>
                    <div className="space-y-5 xl:col-span-3">
                      <div className="space-y-6">
                        <div>
                          <h2 className="text-2xl font-bold leading-8 tracking-tight">
                            <Link
                              href={`/blog/${slug}`}
                              className="text-gray-900 dark:text-gray-100"
                            >
                              {title}
                            </Link>
                          </h2>
                          <div className="flex flex-wrap">
                            {tags.map((tag) => (
                              <Tag key={tag} text={tag} />
                            ))}
                          </div>
                        </div>
                        <div className="prose max-w-none text-gray-500 dark:text-gray-400">
                          {summary}
                        </div>
                      </div>
                      <div className="text-base font-medium leading-6">
                        <Link
                          href={`/blog/${slug}`}
                          className="text-primary-500 hover:text-primary-600 dark:hover:text-primary-400"
                          aria-label={`Read more: "${title}"`}
                        >
                          Read more &rarr;
                        </Link>
                      </div>
                    </div>
                  </div>
                </article>
              </li>
            )
          })}
        </ul>
      </div>
      {posts.length > MAX_DISPLAY && (
        <div className="flex justify-end text-base font-medium leading-6">
          <Link
            href="/blog"
            className="text-primary-500 hover:text-primary-600 dark:hover:text-primary-400"
            aria-label="All posts"
          >
            All Posts &rarr;
          </Link>
        </div>
      )}
      {siteMetadata.newsletter?.provider && (
        <div className="flex items-center justify-center pt-4">
          <NewsletterForm />
        </div>
      )}
    </>
  )
}
