interface Project {
  title: string
  description: string
  href?: string
  imgSrc?: string
}

const projectsData: Project[] = [
  {
    title: 'Egg',
    description: `EGG is an all-in-one platform for crypto and NFT trading, liquidity mining, staking, and wallet management. Users can create a shortlink to showcase their NFT collection and enjoy non-custodial and free services for tracking multiple wallets and generating yield.`,
    imgSrc: '/static/images/egg.png',
    href: 'https://egg.fi',
  },
  {
    title: 'Skyrush',
    description: `Engage qualified leads with viral competitions or random draws and test knowledge with quizzes. Build at record speed and track results in real-time.`,
    imgSrc: '/static/images/skyrush.png',
    href: 'https://skyrush.io',
  },
  {
    title: 'Vave',
    description: `Invest in real estate with Vave starting from $10 through private club deals owned by project managers. Achieve an estimated yield of +9% per year.`,
    imgSrc: '/static/images/vave.png',
    href: 'https://vave.io',
  },
  {
    title: 'DBrush',
    description: `Top -selling photo frames include God, Freedom Fighters & Warriors, Modern Art, and Motivational. Browse their new arrivals sorted by wide range of categories.`,
    imgSrc: '/static/images/dbrush.png',
    href: 'https://dbrush.in/',
  },
]

export default projectsData
